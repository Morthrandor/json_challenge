﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField]
    private Text _titleText;
    [SerializeField]
    private GameObject _dataGridContainer;
    [SerializeField]
    private GameObject _columnText;
    private GridLayoutGroup _gridLayout;
    [SerializeField]
    private GameObject _dataText;


    public void Start()
    {
        _gridLayout = _dataGridContainer.GetComponent<GridLayoutGroup>();
        if (_gridLayout == null)
        {
            Debug.LogError("The gridLayout is null");
        }
    }


    public void ShowTitle(string text)
    {
        _titleText.text = text;
    }


    public void ShowColumnsNames(string[] columnNameText)
    {
        _gridLayout.constraintCount = columnNameText.Length;

        foreach (var columnHeader in columnNameText)
        {
            GameObject column = Instantiate(_columnText);
            Text _text = column.GetComponent<Text>();
            _text.text = columnHeader;
            column.transform.SetParent(_dataGridContainer.transform);
        }
    }

    public void ShowDataText(string[] columnHeaders, Dictionary<string, string>[] data)
    {
        foreach (var record in data)
        {
            for (int i = 0; i < columnHeaders.Length; i++)
            {
                string value = "";
                if (record.ContainsKey(columnHeaders[i]))
                {
                    value = record[columnHeaders[i]];
                }
                GameObject field = Instantiate(_dataText);
                Text _text = field.GetComponent<Text>();
                _text.text = value;
                field.transform.SetParent(_dataGridContainer.transform);
            }
        }
    }
}
