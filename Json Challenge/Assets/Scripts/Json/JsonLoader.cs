﻿using Newtonsoft.Json;
using System.IO;

public static class JsonLoader
{
    public static JsonData Load(string path)
    {
        string jsonFileData = ReadFile(path);

        return JsonConvert.DeserializeObject<JsonData>(jsonFileData);
    }


    private static string ReadFile(string path)
    {
        using (StreamReader reader = new StreamReader(path))
        {
            string fileData = reader.ReadToEnd();
            reader.Close();

            return fileData;
        }
    }
}
