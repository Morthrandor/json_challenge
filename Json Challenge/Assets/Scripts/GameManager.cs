﻿using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private UIManager _uIManager;

    private void Start() 
    {
        _uIManager = GameObject.Find("Canvas").GetComponent<UIManager>();
        if (_uIManager == null)
        {
            Debug.LogError("The UI manager is null");
        }

        StartProcess();
    }


    void Update()
    {
        if (Input.GetKey("escape"))
        {
            Application.Quit();
        }	
    }


    public void StartProcess()
    {
        string path = Path.Combine(Application.streamingAssetsPath, "JsonChallenge.json");

        var fileData = JsonLoader.Load(path);

        _uIManager.ShowTitle(fileData.Title);

        _uIManager.ShowColumnsNames(fileData.ColumnHeaders);

        _uIManager.ShowDataText(fileData.ColumnHeaders, fileData.Data);
    }


    public void Restart()
    {
        SceneManager.LoadScene(0);
    }
}
