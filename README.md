### Defining tasks plan ###

* Create base proyect and scene
	* Hit "Esc" to exit
	* Start process on scene load
* Read Json file
	* Create Json file with test data
	* Json file must be editable on run time (mustn't be included as game asset on build time)
	* Create dynamic serialization struct to support json data
	* Load Json file into memory
* Create UI grid to show Json Data
	* Create UI elements
		* Create Canvas
		* Create title text component
		* Create Columns name text prefab
		* Create Columns data text prefab
		* Create Columms name prefab container
		* Create Columms data prefab container
		* Create reload button
			* Reload buton must trigger point 1.2
* Read Json Data to UI
	* Load Json data Title text
	* Get Columns name length ad create columns prefabs on UI
	* Assign Columns name to UI
	* Read records of Columns data
		* Create prefabs for each test data
		* Assign collumns data to UI 
